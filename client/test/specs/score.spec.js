import * as SCORE from '../../score.js';

describe('Score', () => {
    it('Should crete score enum with GOLD and MONSTER ', () => {
        expect(SCORE).toBeDefined();
        expect(SCORE.GOLD.name).toBe('GOLD');
        expect(SCORE.MONSTER.name).toBe('MONSTER');
    });
});
