class Player {
    constructor(position = {x:0,y:0},health=5,score=0){
        this.position = position;
        this.health = health;
        this.score = score;
    };
};

module.exports = Player;