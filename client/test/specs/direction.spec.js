import * as DIRECTION from '../../direction.js';

describe('DIRECTION', () => {
    it('Should crete DIRECTION enum with E,W,N,S ', () => {
        expect(DIRECTION).toBeDefined();
        expect(DIRECTION.E.name).toBe('E');
        expect(DIRECTION.W.name).toBe('W');
        expect(DIRECTION.N.name).toBe('N');
        expect(DIRECTION.S.name).toBe('S');
    });
});