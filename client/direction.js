import {Enum} from 'enumify';
class Direction extends Enum {};
Direction.initEnum(['E', 'W', 'N','S']);
module.exports = Direction;
