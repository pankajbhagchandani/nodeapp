import Player from '../../player.js';

describe('Player', () => {
    it('Should crete player on default health and score ', () => {
        let player = new Player();
        expect(player.health).toEqual(5);
        expect(player.score).toEqual(0);
    });
    it('Should create a default player with default position as 0,0 ', () => {
        let player = new Player();
        expect(player.position.x).toEqual(0);
        expect(player.position.y).toEqual(0);
    });

    it('Should create a player based on params ', () => {
        let player = new Player({x:1,y:1},5,10);
        expect(player.position.x).toEqual(1);
        expect(player.position.y).toEqual(1);
        expect(player.health).toEqual(5);
        expect(player.score).toEqual(10);
    });
});

