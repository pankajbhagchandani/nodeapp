import {Enum} from 'enumify';
class Score extends Enum {};
Score.initEnum(['GOLD','MONSTER']);
module.exports = Score;
