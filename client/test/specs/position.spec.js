import Position from '../../position.js';

describe('Position', () => {
    it('Should crete position object with default ', () => {
        let position = new Position();
        expect(position.x).toEqual(0);
        expect(position.y).toEqual(0);
    });
});

