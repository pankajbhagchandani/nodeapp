import * as Direction from './direction.js';
import * as SCORE from './score.js'
class ExecuteService {
    movePlayer(player, direction) {
        switch (direction) {
            case Direction.E.name :
                player.position.x++;
                break;
            case Direction.W.name :
                player.position.x--;
                break;
            case Direction.N.name :
                player.position.y++;
                break;
            case Direction.S.name :
                player.position.y--;
                break;
            default :
                console.log('Invalid direction. Direction is in capital E,W,N,S');
                return new Promise((resolve, reject) => {
                    reject("Invalid direction");
                });
        };
        return this.calculateScore(player);
    };

    calculateScore(player) {
        return this.getPositionScore(player).then(function (data) {
            console.log('You have collected ' + data);
                switch (data) {
                case SCORE.GOLD.name :
                    player.score++;
                    break;
                case SCORE.MONSTER.name :
                    player.health--;
                    break;
            };
            console.log('Player health :' + player.health);
            console.log('Player score :' + player.score);
        });
    };

    getPositionScore(player) {
        const Promise = require('es6-promise').Promise;
        let RestClient = require('node-rest-client').Client;
        let rest = new RestClient();
        var args = {
            path: {
                x_coordinate: player.position.x,
                y_coordinate: player.position.y
            },
        };
        return new Promise((resolve, reject) => {
            rest.get("http://localhost:3000/position/${x_coordinate}/${y_coordinate}", args,
                function (data) {
                    resolve(data.toString());
                }).on('error', function (error) {
                console.log('Error with servr. Please check if server is running. Exit from game');
                reject('Error with request'+error);
            });
        });
    };
}
;
module.exports = ExecuteService;
