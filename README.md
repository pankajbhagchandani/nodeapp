# README #

This README would normally document whatever steps are necessary to get your application up and running.

#This is the respository for Node console base application with jasmine test cases.

### How do I get set up? ###

This application has two parts client and server. The application has been completed in two and half hours.
## Directories
#client
#server
Getting started:

npm install(open a new terminal) : To install all npm modules. This is just once.
npm shrink wrap has been created to lock down all npm module version.

npm run test : This will execute all unit test on client side.

npm run startServer (open a new terminal) : This will start the stub server at port 3000 on local host. Leave this server running.

npm start (open a new terminal) : This will start the client application. Player will be by default on 0,0
This will start the Game for you.

Message on screen:
    Game has started. Initial Position 0 0!
    Please enter E, W, N or S to move the player. Press CTRL + C to end the game.
    Please enter direction :Press direction (E/W/N/S) and hit enter. It will display the score and health.Again hit enter for new prompt.


npm run testServer : This will execute server side unit test case to ensure server return data as required. Server should be up and running
for successful execution.

### Assumptions:
Use of npm libraries for certain tasks.
npm as package manager and for task execution. Not used gulp or webpack for simplicity.
The server return GOLD or MONSTER randomly and no specific business logic. This is just like a stub server returning data on request.
End to End test cases have not been added due to time constraints. However all unit test cases have been added.
Additional error handling can be added. 

### Contribution guidelines ###
Each class o have corresponding unit tests /spec.

### Who do I talk to? ###
Pankaj Bhagchandani.
