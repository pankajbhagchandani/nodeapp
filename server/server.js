var express = require('express');
var app     = express();

app.get("/", function(req, res) {
    res.send("Hello!! This is the stub server");
});
app.get('/position/:x/:y', function(req, res, next) {
    var  points = ["GOLD", "MONSTER", "GOLD", "MONSTER"];
    var output = points[Math.floor(Math.random() * points.length)];
    res.send(output);
});

app.listen(3000);
console.log("Server started and listening on 3000");