var request = require("request");

var base_url = "http://localhost:3000/";

describe("Stub Server", function() {
    describe("GET /", function() {
        it("returns status code 200", function(done) {
            request.get(base_url, function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });

    });
    describe("GET /psoition/1/2", function() {
        it("returns status code 200 and response body as text GOLD or MONSTER", function(done) {
            var pos_url = `${base_url}position/1/2`;
            request.get(pos_url, function(error, response, body) {
                var points = (body === 'GOLD' || body === "MONSTER");
                expect(points).toBe(true);
                done();
            });
        });

    });
});