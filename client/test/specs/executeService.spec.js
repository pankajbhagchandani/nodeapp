import Execute from '../../executeService.js';
import * as Direction from '../../direction.js';
describe('Execute Service move player', () => {
    let execute, mockPlayer,client;
    const Promise = require('es6-promise').Promise;
    beforeEach(function(){
        execute = new Execute();

        mockPlayer = {
            position: {x: 0, y: 0},
            health: 5, score: 0
        };
        spyOn(execute, "calculateScore").and.callFake(function () {
            return true;
        });
    });
    it('Execute North should increase the y position by 1', () => {
        execute.movePlayer(mockPlayer, Direction.N.name);
        expect(mockPlayer.position.y).toEqual(1);
    });
    it('Execute South should decrease the y position by 1', () => {
        execute.movePlayer(mockPlayer, Direction.S.name);
        expect(mockPlayer.position.y).toEqual(-1);
    });
    it('Execute East should increase the x position by 1', () => {
        execute.movePlayer(mockPlayer, Direction.E.name);
        expect(mockPlayer.position.x).toEqual(1);
    });
    it('Execute South should decrease the y position by 1', () => {
        execute.movePlayer(mockPlayer, Direction.W.name);
        expect(mockPlayer.position.x).toEqual(-1);
    });
});
describe('Execute Service calculate score', () => {
    let execute, mockPlayer;
    beforeEach(function(){
        execute = new Execute();
        mockPlayer = {
            position: {x: 0, y: 0},
            health: 5, score: 0
        };

        spyOn(execute, "getPositionScore").and.callFake(function () {
            return new Promise((resolve, reject) => {
                resolve('GOLD');
            });
        });

    });
    it('Should increase score by 1 if service return GOLD', (done) => {
        execute.calculateScore(mockPlayer).then(function(data){
            expect(mockPlayer.health).toEqual(5);
            expect(mockPlayer.score).toEqual(1);
            done();
        });
    });
});

describe('Execute Service calculate score', () => {
    let execute, mockPlayer;
    beforeEach(function(){
        execute = new Execute();
        mockPlayer = {
            position: {x: 0, y: 0},
            health: 5, score: 0
        };

        spyOn(execute, "getPositionScore").and.callFake(function () {
            return new Promise((resolve, reject) => {
                resolve('MONSTER');
            });
        });

    });
    it('Should decrease health by 1 if service return MONSTER', (done) => {
        execute.calculateScore(mockPlayer).then(function(health){
            expect(mockPlayer.health).toEqual(4);
            expect(mockPlayer.score).toEqual(0);
            done();
        });

    });
});
