import app from '../../app.js';
import Execute from '../../executeService.js';
describe('App', () => {
    it('start method should call play method', () => {
        let executeService = new Execute();
        let prompt = {
            start: function () {

            }
        };
        let application = new app(executeService, prompt);
        let player = {
            position: {x: 0, y: 0}, health: 5, score: 0
        };
        spyOn(prompt, "start").and.callFake(function (player) {
            return true;
        });
        expect(application).toBeDefined();
        spyOn(application, "play").and.callFake(function (player) {
            return new Promise((resolve, reject) => {
                resolve(player);
            });

        });
        application.start(player);
        expect(application.play).toHaveBeenCalled();
    });
    it('end method should end method of prompt', () => {
        let executeService = new Execute();
        let fakeprompt = {
            stop: function () {

            }
        };
        let application = new app(executeService, fakeprompt);
        let player = {
            position: {x: 0, y: 0}, health: 5, score: 0
        };
        spyOn(fakeprompt, "stop").and.callFake(function () {
            return;
        });
        expect(application).toBeDefined();
        application.end(player);
        expect(fakeprompt.stop).toHaveBeenCalled();
    });
    it('play method should call play method of Execute service', (done) => {
        let mockExecuteService = {
            movePlayer: function (player, direction) {

            }
        };
        let mockPrompt = {
            start: function () {

            },
            get: function () {

            }
        };
        let player = {
            position: {x: 0, y: 0}, health: 5, score: 0
        };
        spyOn(mockPrompt, "get").and.callFake(function () {
            return new Promise((resolve, reject) => {
                resolve('E');
            });
        });
        spyOn(mockExecuteService, "movePlayer").and.callFake(function () {
            return new Promise((resolve, reject) => {
                resolve(player);
            });
        });
        let application = new app(mockExecuteService, mockPrompt);
        application.play(player);
        done();
        expect(mockPrompt.get).toHaveBeenCalled();
        expect(mockExecuteService.movePlayer).toHaveBeenCalled();
    });
});