import prompt from 'prompt';
import Player from './player.js';
import Execute from './executeService.js';
class app {
    constructor(execute, prompt) {
        this.execute = execute;
        this.prompt = prompt;
    };

    start(player) {
        this.prompt.start();
        this.prompt.message = 'Please enter';
        let message = `Game has started. Initial Position ${player.position.x} ${player.position.y}!`;
        console.log(message);
        console.log("Please enter E, W, N or S to move the player. Press CTRL + C to end the game");
         this.play(player);
    };

    play(player) {
        let current = this;
        if (player.health > 0) {
            return new Promise((resolve, reject) => {
                current.prompt.get(['direction'], function (err, result) {
                    console.log(result.direction);
                    current.execute.movePlayer(player, result.direction).then(function (data) {
                        let message = `New Pos X:${player.position.x} Y:${player.position.y}!`;
                        console.log(message);
                        return current.play(player);
                    });
                });
            });
        } else {
            current.end(player);
        }
    };

    end(player) {
        let message = `Game over..Final score is ${player.score}`;
        console.log(message);
        this.prompt.stop();
    };
}
;
module.exports = app;
let player = new Player();
const execute = new Execute();
var application = new app(execute, prompt);
application.start(player);